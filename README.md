### TaskMate - Task Tracker

Ever wanted to keep track of all your tasks your pesky manager gives you?

Look no further! with TaskMate you have all your tasks directly at your fingertips.

#### Some Features:

* All your tasks are stored on your local device so you don't need an internet 
connection to sync everything. 

* Quick type to add your task to your list. 

* Swipe to delete your tasks when you no longer need them, otherwise keep them 
cancelled out if you would like to refer to them later.

* Deleted a task by mistake? no problem! When you swipe to delete, just tap undo
and your task instantly reappears!

#### For the technically minded. 


* The Room database was used to ensure all tasks are kept local to the device. So
no internet connection required! (crazy right?) 

* The app uses the MVVM architecture to ensure separation of concerns are adhered to. 

* Instead of importing a whole RxJava lib to facilitate threading, we made use of
Kotlin coroutines to help with this. They're lightweight and require less code.

* we've covered the DB queries with tests to ensure everything is working as advertised. 

Visual person? no problem! We've even got a diagram of what this looks like below. 

![App Arch](https://generic-store-eu.s3.eu-west-2.amazonaws.com/arch.jpeg)

#### A Quick primer:

1. UI - This is what the user will interact with. A fairly basic UI

2. ViewModel - We use the AndroidViewModel here as we need the application context
for your database. Manual dependency injection used her.

3. Repository layer. Small app yes but the practise of keeping our layers with their respective
responsibilities is important

4. Cache layer - This is where we implement our caching layer, in this case our SQLite DB. 
we use the Room library to back this.

5. DB - backed by the Room library. We use our LiveData to subscribe to all DB events here. Coroutines
are also implemented here

### Opening the project

You can clone this repo directly, then open the root-level build.gradle file. This should
be able to let you build and run the project

* Project minSDK is 21
* Android Studio version used: 3.6.1

