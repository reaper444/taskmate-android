package za.co.kernelpanic.taskmate.factory

import za.co.kernelpanic.taskmate.data.model.Task


/**
 * @author: vusimoyo
 * @created: 15:59 02/03/2020
 */

object TaskMateFactory {

    fun makeTask(): Task {
        return Task(id = DataFactory.randomInt(), taskName = DataFactory.randomString(),
            isDone = DataFactory.randomBoolean())
    }

    fun makeTaskList(numberOfTasks: Int): List<Task> {
        val taskList = mutableListOf<Task>()
        repeat(numberOfTasks){
            taskList.add(makeTask())
        }
        return taskList.toList()
    }
}