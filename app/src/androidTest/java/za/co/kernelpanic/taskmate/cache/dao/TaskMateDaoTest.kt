package za.co.kernelpanic.taskmate.cache.dao

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import za.co.kernelpanic.taskmate.data.model.Task
import za.co.kernelpanic.taskmate.factory.TaskMateFactory
import za.co.kernelpanic.taskmate.repository.cache.dao.TaskMateDao
import za.co.kernelpanic.taskmate.repository.cache.db.TaskMateDatabase
import za.co.kernelpanic.taskmate.util.CoroutinesTestRule
import za.co.kernelpanic.taskmate.util.getOrAwaitValue

/**
 * @author: vusimoyo
 * @created: 15:56 02/03/2020
 */

@ExperimentalCoroutinesApi
class TaskMateDaoTest {

    @Rule
    @JvmField
    var instantTaskExecutionRule = InstantTaskExecutorRule()

    @get:Rule
    val coroutineTestRule = CoroutinesTestRule()

    private lateinit var taskMateDao: TaskMateDao
    private lateinit var db: TaskMateDatabase

    @Before
    fun setup() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, TaskMateDatabase::class.java).build()
        taskMateDao = db.tasksDao()
    }

    @Test
    fun insertTaskSavesData() {
        val task = TaskMateFactory.makeTask()
        coroutineTestRule.testDispatcher.runBlockingTest {
            taskMateDao.insertTask(task)
            assertEquals(taskMateDao.retrieveTasks().getOrAwaitValue()[0], task)
        }
    }

    @Test
    fun deleteTaskDeletesData() {
        val task = TaskMateFactory.makeTask()
        coroutineTestRule.testDispatcher.runBlockingTest {
            taskMateDao.insertTask(task)
            taskMateDao.deleteTaskById(task.id)
            assertEquals(taskMateDao.retrieveTasks().getOrAwaitValue(), emptyList<Task>())
        }
    }

    @Test
    fun updatingTaskUpdatesData() {
        val task = TaskMateFactory.makeTask()
        coroutineTestRule.testDispatcher.runBlockingTest {
            taskMateDao.insertTask(task)
            task.isDone = true
            taskMateDao.markTaskComplete(task)
            assertEquals(taskMateDao.retrieveTasks().getOrAwaitValue()[0].isDone, true)
        }
    }

    @After
    fun tearDown() = db.close()
}