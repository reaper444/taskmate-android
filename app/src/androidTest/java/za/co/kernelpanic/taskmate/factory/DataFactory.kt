package za.co.kernelpanic.taskmate.factory

import java.util.*
import java.util.concurrent.ThreadLocalRandom

/**
 * @author: vusimoyo
 * @created: 16:02 02/03/2020
 */
object DataFactory {

    fun randomString(): String = UUID.randomUUID().leastSignificantBits.toString()

    fun randomInt(): Int = ThreadLocalRandom.current().nextInt(0, 1000 + 1)

    fun randomBoolean(): Boolean = Math.random() < 0.5
}