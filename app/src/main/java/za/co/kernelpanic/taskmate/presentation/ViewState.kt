package za.co.kernelpanic.taskmate.presentation

/**
 * @author: vusimoyo
 * @created: 14:05 02/03/2020
 */

enum class ViewState {
    INIT, LOADING, LOADED, ERROR
}