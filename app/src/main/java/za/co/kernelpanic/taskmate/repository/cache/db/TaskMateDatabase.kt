package za.co.kernelpanic.taskmate.repository.cache.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import za.co.kernelpanic.taskmate.data.model.Task
import za.co.kernelpanic.taskmate.repository.cache.dao.TaskMateDao

/**
 * @author: vusimoyo
 * @created: 19:39 27/02/2020
 */

@Database(entities = [Task::class], version = 1, exportSchema = false)
abstract class TaskMateDatabase : RoomDatabase() {

    abstract fun tasksDao(): TaskMateDao

    companion object {
        private var INSTANCE: TaskMateDatabase? = null
        private var LOCK = Any()

        fun getInstance(context: Context): TaskMateDatabase {
            if (INSTANCE == null) {
                synchronized(LOCK) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            TaskMateDatabase::class.java,
                            "taskmate.db"
                        )
                            .build()
                    }
                    return INSTANCE as TaskMateDatabase
                }
            }
            return INSTANCE as TaskMateDatabase
        }
    }
}