package za.co.kernelpanic.taskmate.repository

import androidx.lifecycle.LiveData
import za.co.kernelpanic.taskmate.data.model.Task

/**
 * @author: vusimoyo
 * @created: 19:47 27/02/2020
 */
interface TaskMateRepo {

    suspend fun deleteTasks()

    suspend fun deleteTaskById(id: Int)

    suspend fun insertTask(task: Task)

    suspend fun markTaskComplete(task: Task)

    fun getAllTasks(): LiveData<List<Task>>
}