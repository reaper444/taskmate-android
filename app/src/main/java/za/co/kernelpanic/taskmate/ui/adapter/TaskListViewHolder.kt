package za.co.kernelpanic.taskmate.ui.adapter

import android.graphics.Paint
import androidx.recyclerview.widget.RecyclerView
import za.co.kernelpanic.taskmate.data.model.Task
import za.co.kernelpanic.taskmate.databinding.ListItemTasksBinding

/**
 * @author: vusimoyo
 * @created: 21:49 27/02/2020
 */

class TaskListViewHolder(
    private val binding: ListItemTasksBinding,
    private val taskCompleteSelectedListener: TaskCompleteSelectedListener
) :
    RecyclerView.ViewHolder(binding.root) {

    internal var backgroundView = binding.viewBackground
    internal var foregroundView = binding.constraintLayoutForegroundRecyclerView

    fun bind(task: Task) {
        binding.taskNameTextView.text = task.taskName
        binding.taskDoneCheckBox.isChecked = task.isDone

        binding.taskDoneCheckBox.setOnCheckedChangeListener { _, isChecked ->
            task.isDone = isChecked
            taskCompleteSelectedListener.markTaskComplete(task)
            if (isChecked) {
                binding.taskNameTextView.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
            } else {
                binding.taskNameTextView.paintFlags = 0
            }
        }

        if (task.isDone) {
            binding.taskNameTextView.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG
        } else {
            binding.taskNameTextView.paintFlags = 0
        }
    }
}