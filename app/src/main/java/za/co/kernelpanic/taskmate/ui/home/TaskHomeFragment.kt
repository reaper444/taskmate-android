package za.co.kernelpanic.taskmate.ui.home

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.*
import com.google.android.material.snackbar.Snackbar
import za.co.kernelpanic.taskmate.R
import za.co.kernelpanic.taskmate.data.model.Task
import za.co.kernelpanic.taskmate.databinding.FragmentTaskHomeBinding
import za.co.kernelpanic.taskmate.ui.adapter.*
import za.co.kernelpanic.taskmate.util.formatProgress

class TaskHomeFragment : Fragment(), SwipeTouchHelperListener, TaskCompleteSelectedListener {

    private lateinit var binding: FragmentTaskHomeBinding
    private lateinit var taskListAdapter: TaskListRecyclerViewAdapter
    private lateinit var viewModel: TaskMateViewModel
    private lateinit var taskList: List<Task>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_task_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val toolbar: Toolbar = binding.toolBar
        toolbar.title = getString(R.string.text_home_toolbar_title)
        toolbar.setTitleTextColor(resources.getColor(android.R.color.white))
        requireActivity().run {
            viewModel = ViewModelProvider(this)[TaskMateViewModel::class.java]
        }
        setupRecyclerView()
        initObservers()
        initListeners()
    }

    private fun initListeners() {
        binding.addTaskButton.setOnClickListener {
            val task = binding.addTaskEditText.text.toString().trim()
            if (task.isNotEmpty()) {
                hideKeyboard()
                viewModel.insertTask(Task(taskName = task))
                binding.addTaskEditText.text = null
            }
        }
    }

    private fun setupRecyclerView() {
        binding.taskListRecyclerView.layoutManager = LinearLayoutManager(requireContext())
        binding.taskListRecyclerView.itemAnimator = DefaultItemAnimator()
        binding.taskListRecyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                DividerItemDecoration.VERTICAL
            )
        )
        val itemTouchHelper = ItemTouchHelper(SwipeTouchHelper(this))
        itemTouchHelper.attachToRecyclerView(binding.taskListRecyclerView)
        taskListAdapter = TaskListRecyclerViewAdapter(this)
        binding.taskListRecyclerView.adapter = taskListAdapter
    }

    private fun initObservers() {
        viewModel.allTasksLiveData.observe(viewLifecycleOwner, Observer { taskList ->
            this.taskList = taskList.orEmpty()
            if (taskList.isEmpty()) {
                binding.constraintGroup.visibility = View.VISIBLE
                binding.taskListRecyclerView.visibility = View.GONE
                binding.include.constraintLayoutTaskProgress.visibility = View.GONE
            } else {
                binding.constraintGroup.visibility = View.GONE
                binding.taskListRecyclerView.visibility = View.VISIBLE
                binding.include.constraintLayoutTaskProgress.visibility = View.VISIBLE
                taskListAdapter.showTasks(taskList.toMutableList())
                updateProgress(taskList)
            }
        })
    }

    private fun updateProgress(tasks: List<Task>) {

        binding.include.tasksCompleteProgresssBar.max = 100
        var doneTasks = 0.0
        tasks.forEach { task ->
            if (task.isDone) {
                doneTasks++
            }
        }

        val taskProgress = (doneTasks / tasks.size.toDouble()) * 100.00
        binding.include.tasksCompleteProgresssBar.progress = taskProgress.toInt()
        binding.include.percentageDoneTextView.text =
            getString(R.string.tasks_progress_text, taskProgress.formatProgress())
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int) {
        if (viewHolder is TaskListViewHolder) {
            val taskName = taskList[viewHolder.adapterPosition].taskName
            val task = taskList[viewHolder.adapterPosition]
            val deletedItem = taskList[viewHolder.adapterPosition]
            val deletedIndex = viewHolder.adapterPosition

            taskListAdapter.deleteItem(viewHolder.adapterPosition)
            viewModel.deleteTask(task)

            val snackBar = Snackbar.make(
                binding.coordinatorLayoutParent, getString(R.string.text_undo_delete, taskName),
                Snackbar.LENGTH_LONG
            )

            snackBar.setAction(getString(R.string.swipe_text_undo)) {
                taskListAdapter.undoDelete(deletedItem, deletedIndex)
                viewModel.insertTask(task)
            }

            snackBar.setActionTextColor(Color.YELLOW)
            snackBar.show()
        }
    }

    override fun markTaskComplete(task: Task) {
        viewModel.markTaskComplete(task)
    }

    private fun hideKeyboard() {
        val keyboard: InputMethodManager =
            requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        keyboard.hideSoftInputFromWindow(binding.addTaskEditText.windowToken, 0)
    }
}
