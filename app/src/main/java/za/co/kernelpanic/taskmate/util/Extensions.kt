package za.co.kernelpanic.taskmate.util

import java.text.DecimalFormat

/**
 * @author: vusimoyo
 * @created: 20:33 27/02/2020
 */

fun Double.formatProgress(): String  {
    val df = DecimalFormat("#.##")
    return df.format(this)
}
