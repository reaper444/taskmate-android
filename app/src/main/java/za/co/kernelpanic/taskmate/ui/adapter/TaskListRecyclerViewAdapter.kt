package za.co.kernelpanic.taskmate.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import za.co.kernelpanic.taskmate.R
import za.co.kernelpanic.taskmate.data.model.Task
import za.co.kernelpanic.taskmate.databinding.ListItemTasksBinding

/**
 * @author: vusimoyo
 * @created: 21:49 27/02/2020
 */

class TaskListRecyclerViewAdapter(private val listener: TaskCompleteSelectedListener) :
    RecyclerView.Adapter<TaskListViewHolder>() {

    private var listOfTasks = mutableListOf<Task>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskListViewHolder {
        val binding: ListItemTasksBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.list_item_tasks, parent, false
        )
        return TaskListViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: TaskListViewHolder, position: Int) {
        val userTask = listOfTasks[position]
        holder.bind(userTask)
    }

    fun showTasks(taskList: MutableList<Task>) {
        listOfTasks = taskList
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int) {
        listOfTasks.removeAt(position)
        notifyItemRemoved(position)
    }

    fun undoDelete(deletedItem: Task, deletedIndex: Int) {
        listOfTasks.add(deletedIndex, deletedItem)
        notifyItemInserted(deletedIndex)
    }

    override fun getItemCount(): Int = listOfTasks.size
}