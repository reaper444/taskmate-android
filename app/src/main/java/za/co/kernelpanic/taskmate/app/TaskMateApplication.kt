package za.co.kernelpanic.taskmate.app

import android.app.Application

/**
 * @author: vusimoyo
 * @created: 20:26 27/02/2020
 */
class TaskMateApplication : Application() {

    companion object {
        private lateinit var applicationInstance: TaskMateApplication

        fun getInstance(): TaskMateApplication {
            return applicationInstance
        }
    }

    override fun onCreate() {
        super.onCreate()
        applicationInstance = this
    }
}