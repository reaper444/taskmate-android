package za.co.kernelpanic.taskmate.ui.adapter

import za.co.kernelpanic.taskmate.data.model.Task

interface TaskCompleteSelectedListener {

    fun markTaskComplete(task: Task)
}