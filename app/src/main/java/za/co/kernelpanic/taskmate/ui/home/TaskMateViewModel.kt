package za.co.kernelpanic.taskmate.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import za.co.kernelpanic.taskmate.app.TaskMateApplication
import za.co.kernelpanic.taskmate.data.model.Task
import za.co.kernelpanic.taskmate.di.Injection

/**
 * @author: vusimoyo
 * @created: 20:12 27/02/2020
 */
class TaskMateViewModel(application: Application) : AndroidViewModel(application) {

    private val repo = Injection(TaskMateApplication.getInstance()).provideTaskMateRepo()

    val allTasksLiveData = repo.getAllTasks()

    fun insertTask(task: Task) = viewModelScope.launch {
        repo.insertTask(task)
    }

    fun deleteTask(task: Task) = viewModelScope.launch {
        repo.deleteTaskById(task.id)
    }

    fun markTaskComplete(task: Task) =
        viewModelScope.launch(Dispatchers.Main) {
            repo.markTaskComplete(task)
        }

    override fun onCleared() {
        super.onCleared()
    }
}