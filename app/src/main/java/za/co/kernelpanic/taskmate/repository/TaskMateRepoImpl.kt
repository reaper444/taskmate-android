package za.co.kernelpanic.taskmate.repository

import androidx.lifecycle.LiveData
import za.co.kernelpanic.taskmate.data.model.Task
import za.co.kernelpanic.taskmate.repository.cache.dao.TaskMateDao

/**
 * @author: vusimoyo
 * @created: 19:50 27/02/2020
 */

class TaskMateRepoImpl constructor(private val taskMateDao: TaskMateDao) : TaskMateRepo {

    override suspend fun deleteTasks() = taskMateDao.deleteTasks()

    override suspend fun deleteTaskById(id: Int) = taskMateDao.deleteTaskById(id)

    override suspend fun insertTask(task: Task) = taskMateDao.insertTask(task)

    override suspend fun markTaskComplete(task: Task) = taskMateDao.markTaskComplete(task)

    override fun getAllTasks(): LiveData<List<Task>> = taskMateDao.retrieveTasks()
}