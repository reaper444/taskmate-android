package za.co.kernelpanic.taskmate.repository.cache.db

/**
 * @author: vusimoyo
 * @created: 16:23 27/02/2020
 */
object DbConstants {

    const val TABLE_NAME = "tasks"

    const val COLUMN_TASK_NAME = "task_name"

    const val COLUMN_TASK_DESCRIPTION = "task_description"

    const val COLUMN_TASK_IS_COMPLETE = "task_complete"

    const val QUERY_RETRIEVE_TASKS = "SELECT * FROM $TABLE_NAME"

    const val QUERY_MARK_TASK_COMPLETE = "UPDATE $TABLE_NAME SET $COLUMN_TASK_IS_COMPLETE = :isDone WHERE id = :taskId"

    const val QUERY_DELETE_TASK_BY_ID = "DELETE FROM $TABLE_NAME WHERE id = :id"

    const val QUERY_GET_COMPLETE_TASKS =
        "SELECT * FROM $TABLE_NAME WHERE $COLUMN_TASK_IS_COMPLETE = :isComplete"

    const val QUERY_DELETE_TASKS = "DELETE FROM $TABLE_NAME"
}