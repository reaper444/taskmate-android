package za.co.kernelpanic.taskmate.ui.adapter

import androidx.recyclerview.widget.RecyclerView

interface SwipeTouchHelperListener {

    fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int, position: Int)
}