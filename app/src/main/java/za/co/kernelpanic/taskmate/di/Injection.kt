package za.co.kernelpanic.taskmate.di

import android.content.Context
import za.co.kernelpanic.taskmate.repository.TaskMateRepo
import za.co.kernelpanic.taskmate.repository.TaskMateRepoImpl
import za.co.kernelpanic.taskmate.repository.cache.dao.TaskMateDao
import za.co.kernelpanic.taskmate.repository.cache.db.TaskMateDatabase

/**
 * @author: vusimoyo
 * @created: 20:16 27/02/2020
 */
class Injection (private val context: Context) {

    fun provideTaskMateRepo(): TaskMateRepo {
        return TaskMateRepoImpl(getTaskMateDao())
    }

    private fun getTaskMateDao(): TaskMateDao {
        return TaskMateDatabase.getInstance(context).tasksDao()
    }
}