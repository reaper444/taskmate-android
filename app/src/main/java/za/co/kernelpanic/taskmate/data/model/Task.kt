package za.co.kernelpanic.taskmate.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import za.co.kernelpanic.taskmate.repository.cache.db.DbConstants

/**
 * @author: vusimoyo
 * @created: 10:47 27/02/2020
 * @description: Database Entity for our Tasks
 */

@Entity (tableName = DbConstants.TABLE_NAME)
data class Task(
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0,
    @ColumnInfo(name = DbConstants.COLUMN_TASK_NAME)
    val taskName: String,
    @ColumnInfo(name = DbConstants.COLUMN_TASK_IS_COMPLETE)
    var isDone: Boolean = false
)