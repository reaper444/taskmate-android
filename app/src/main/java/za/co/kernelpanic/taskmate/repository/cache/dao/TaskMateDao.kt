package za.co.kernelpanic.taskmate.repository.cache.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import za.co.kernelpanic.taskmate.data.model.Task
import za.co.kernelpanic.taskmate.repository.cache.db.DbConstants

/**
 * @author: vusimoyo
 * @created: 16:26 27/02/2020
 */

@Dao
interface TaskMateDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTask(task: Task)

    @Query(DbConstants.QUERY_RETRIEVE_TASKS)
    fun retrieveTasks(): LiveData<List<Task>>

    @Update
    suspend fun markTaskComplete(task: Task)

    @Query(DbConstants.QUERY_DELETE_TASKS)
    suspend fun deleteTasks()

    @Query(DbConstants.QUERY_DELETE_TASK_BY_ID)
    suspend fun deleteTaskById(id: Int)
}